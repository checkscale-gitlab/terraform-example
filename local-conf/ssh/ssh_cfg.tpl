Host ${bastion_public_ip}
  IdentityFile ${ssh_key_file}
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  User ec2-user

Host ${ip_glob}
  IdentityFile ${ssh_key_file}
  ProxyCommand ssh -q -i ${ssh_key_file} ec2-user@${bastion_public_ip} nc -w 60 %h %p
  UserKnownHostsFile /dev/null
  IdentitiesOnly  yes
  ControlMaster   auto
  ControlPath     /tmp/.ssh/mux-%r@%h:%p
  ControlPersist  15m
  StrictHostKeyChecking no
  User ec2-user