provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "${var.aws_region}"
}

module "bastion" {
	source 				= "./instances/bastion"
	aws_key_name		= "${var.aws_key_name}"
	user_name			= "${var.user_name}"
	aws_region			= "${var.aws_region}"
	ami					= "${lookup(var.amis, var.aws_region)}"
	vpc_id				= "${var.vpc_id}"
	vpc_name			= "${var.vpc_name}"
	public_subnet_id	= "${var.public_subnet_id}"
}

module "ssh" {
    source 				= "./local-conf/ssh"
    bastion_public_ip 	= "${module.bastion.bastion_public_ip}"
    ip_glob 			= "${var.vpc_global_ip}"
    ssh_key_file 		= "${var.aws_key_path}"
}

module "db" {
	source				= "./instances/db"
	aws_key_name		= "${var.aws_key_name}"
	user_name			= "${var.user_name}"
	ami					= "${lookup(var.amis, var.aws_region)}"
	vpc_id				= "${var.vpc_id}"
	private_subnet_id	= "${var.private_subnet_id}"
	public_subnet_cidr	= "${var.public_subnet_cidr}"
	aws_region			= "${var.aws_region}"
}

resource "null_resource" "provisioning_db" {
	depends_on			= ["module.bastion", "module.ssh"]
    provisioner "local-exec" {
        command     =   "cd ./ansible/terraform/ && sleep 30 && ansible-playbook -i \"${module.db.db_ip},\" -u ec2-user -s -c ssh elastic.yml"
    }
}

module "webserver" {
	source				= "./instances/webserver"
	aws_key_name		= "${var.aws_key_name}"
	user_name			= "${var.user_name}"
	ami					= "${lookup(var.amis, var.aws_region)}"
	vpc_id				= "${var.vpc_id}"
	public_subnet_id	= "${var.public_subnet_id}"
	aws_region			= "${var.aws_region}"
	public_subnet_cidr	= "${var.public_subnet_cidr}"
	count				= "${var.webserver_count}"
}

resource "null_resource" "provisioning_web" {
	depends_on			= ["module.bastion", "module.ssh", "module.db"]
	triggers {
    	webserver_cluster_ips = "${join(",", module.webserver.webserver_ips)}"
  	}
    provisioner "local-exec" {
        command     =   "cd ./ansible/terraform/ && sleep 30 && ansible-playbook -i \"${join(",", module.webserver.webserver_ips)},\" -u ec2-user -s -c ssh --extra-vars '{\"db_ip\" : \"${module.db.db_ip}\"}' nginx.yml"
    }
}